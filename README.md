# Fantasy Coders Academy

## Introduction

This is an interactive website that provides information about the Fantasy Coders Academy. It implements a responsive design and 
incorporate at least one interactive feature with javascript coding and one jQuery plug-in. The content and design elements of the website are 
original and no external templates have been used.

## Preview

[Visit the Website](http://fcaca2.azurewebsites.net/)

## Usage

1. Clone this repository into any directory.

2. Enter the "fantasy-coders-academy" directory by typing `cd fantasy-coders-academy`. 

3. The website is ready.

## Disclaimer

This program was developed as an assignment for the Singapore Polytechnic, ST1008 Web Client Development module. I have uploaded the source files publicly for reference and personal usage only. Please refrain from [plagiarising](https://www.sp.edu.sg/sp/student-services/ssc-overview/student-handbook/intellectual-property-copyright-and-plagiarism) or passing it off as your own work. 

**Developer**

- Chua Han Yong Darren ([@chydarren](https://github.com/chydarren))

## Copyright and License 

Chua Han Yong Darren © 2017. Code released under the GNU Affero General Public License v3.0.
